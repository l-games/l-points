module Player where

import Database.Persist.TH
import Data.Aeson.TH

data Player = Red | Black
  deriving (Eq, Show, Read)

derivePersistField "Player"

$(deriveJSON defaultOptions ''Player)

nextPlayer :: Player -> Player
nextPlayer Red = Black
nextPlayer Black = Red
