module PersistentEnums where

import Database.Persist.TH
import Data.Aeson.TH

data UserRole = UserRoleAdministrator | UserRoleModerator | UserRoleRegular
  deriving (Show, Read, Eq)

data GameResult = GameResultDraw | GameResultRedWins | GameResultBlackWins
  deriving (Show, Read, Eq)

data CompletionCause = CompletionCauseCapitulate | CompletionCauseStop
  deriving (Show, Read, Eq)

derivePersistField "UserRole"

derivePersistField "GameResult"

derivePersistField "CompletionCause"

$(deriveJSON defaultOptions { constructorTagModifier = drop 8 } ''UserRole)

$(deriveJSON defaultOptions { constructorTagModifier = drop 10 } ''GameResult)

$(deriveJSON defaultOptions { constructorTagModifier = drop 15 } ''CompletionCause)
