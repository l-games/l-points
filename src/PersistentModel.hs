module PersistentModel where

import Data.Text
import Data.Time
import Database.Persist.TH
import Database.Persist.Quasi
import Config
import PersistentEnums

share [mkPersist sqlSettings, mkMigrate "migrateAll"]
  $(persistFileWith upperCaseSettings modelsFile)
