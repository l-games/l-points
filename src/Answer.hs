module Answer where

import qualified Data.Text as T
import Data.Aeson
import Data.Aeson.Types
import Data.Aeson.TH
import qualified Data.HashMap.Strict as HM
import PersistentEnums
import GameSettings
import RemovePrefix

data AnswerUser = AnswerUser { answerUserName :: T.Text,
                               answerUserEmail :: T.Text,
                               answerUserRole :: UserRole }

$(deriveJSON defaultOptions { fieldLabelModifier = removePrefix 10 } ''AnswerUser)

data AnswerActiveUser = AnswerActiveUser { answerActiveUserName :: T.Text,
                                           answerActiveUserEmail :: T.Text,
                                           answerActiveUserRole :: UserRole,
                                           answerActiveUserBusy :: Bool }

$(deriveJSON defaultOptions { fieldLabelModifier = removePrefix 16 } ''AnswerActiveUser)

data AnswerGame = AnswerGame { answerGameId :: Int,
                               answerGameRed :: T.Text,
                               answerGameBlack :: T.Text,
                               answerGameSettings :: GameSettings }

$(deriveJSON defaultOptions { fieldLabelModifier = removePrefix 10 } ''AnswerGame)

data ErrorAnswer = ErrorAnswerMessageFormat |
                   ErrorAnswerAccessDenied |
                   ErrorAnswerAlreadyLogined |
                   ErrorAnswerUserAlreadyExists |
                   ErrorAnswerTooSmallUserName |
                   ErrorAnswerTooBigUserName |
                   ErrorAnswerTooSmallPassword |
                   ErrorAnswerTooBigPassword |
                   ErrorAnswerTooSmallMessage |
                   ErrorAnswerTooBigMessage |
                   ErrorAnswerIncorrectPassword |
                   ErrorAnswerUserBanned |
                   ErrorAnswerUserNotFound |
                   ErrorAnswerRoomNotFound |
                   ErrorAnswerRequestAlreadyExists |
                   ErrorAnswerRequestDoesNotExist |
                   ErrorAnswerInvalidSettings

$(deriveJSON defaultOptions { constructorTagModifier = drop 11 } ''ErrorAnswer)

data OkAnswer = OkAnswerProtocolVersion { okAnswerVersion :: Int } |
                OkAnswerRegistered |
                OkAnswerLogined |
                OkAnswerSended |
                OkAnswerRooms { okAnswerRooms :: [T.Text] } |
                OkAnswerSubscribed |
                OkAnswerActiveUsers { okAnswerActiveUsers :: [AnswerActiveUser] } |
                OkAnswerActiveGames { okAnswerActiveGames :: [AnswerGame] } |
                OkAnswerUsersCount { okAnswerUsersCount :: Int } |
                OkAnswerGamesCount { okAnswerGamesCount :: Int } |
                OkAnswerUsers { okAnswerUsers :: [AnswerUser] } |
                OkAnswerGames { okAnswerGames :: [AnswerGame] } |
                OkAnswerRequestCreated |
                OkAnswerRequestCanceled

$(deriveJSON defaultOptions { constructorTagModifier = drop 8,
                              fieldLabelModifier = removePrefix 8,
                              sumEncoding = defaultTaggedObject { tagFieldName = "type" } } ''OkAnswer)

data DeliveryAnswer = DeliveryAnswerRoomMessage { deliveryAnswerRoom :: T.Text, deliveryAnswerText :: T.Text } |
                      DeliveryAnswerRequestCreated { deliveryAnswerUser :: T.Text, deliveryAnswerSettings :: GameSettings } |
                      DeliveryAnswerRequestCanceled { deliveryAnswerUser :: T.Text }

$(deriveJSON defaultOptions { constructorTagModifier = drop 14,
                              fieldLabelModifier = removePrefix 14,
                              sumEncoding = defaultTaggedObject { tagFieldName = "type" } } ''DeliveryAnswer)

data Answer = AnswerError ErrorAnswer |
              AnswerOk OkAnswer |
              AnswerDelivery DeliveryAnswer

fromObject :: Value -> HM.HashMap T.Text Value
fromObject (Object hm) = hm
fromObject _ = error "fromObject: not object."

instance ToJSON Answer where
  toJSON (AnswerError errorAnswer) = object ["ans" .= ("Error" :: T.Text), "type" .= toJSON errorAnswer]
  toJSON (AnswerOk okAnswer) = Object $ HM.insert "ans" (String "Ok") $ fromObject $ toJSON okAnswer
  toJSON (AnswerDelivery deliveryAnswer) = Object $ HM.insert "ans" (String "Delivery") $ fromObject $ toJSON deliveryAnswer
