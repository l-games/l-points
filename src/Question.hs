module Question where

import Control.Applicative
import Control.Monad
import qualified Data.Text as T
import Data.Aeson
import GameSettings

data Question = QuestionProtocolVersion |
                QuestionRegister { questionName :: T.Text, questionPassword :: T.Text, questionEmail :: T.Text } |
                QuestionLogin { questionName :: T.Text, questionPassword :: T.Text } |
                QuestionRoomsList |
                QuestionSubscribeToRoom { questionName :: T.Text } |
                QuestionActiveUsersList |
                QuestionActiveGamesList |
                QuestionRoomMessage { questionName :: T.Text, questionText :: T.Text } |
                QuestionUsersCount |
                QuestionGamesCount |
                QuestionUsersList { questionPage :: Int, questionPageSize :: Int } |
                QuestionGamesList { questionPage :: Int, questionPageSize :: Int } |
                QuestionRequest { questionSettings :: GameSettings } |
                QuestionCancelRequest

instance FromJSON Question where
  parseJSON (Object v) =
    do msg <- v .: "msg"
       case msg :: T.Text of
         "ProtocolVersion" -> return QuestionProtocolVersion
         "Register" -> QuestionRegister <$> v .: "name" <*> v .: "password" <*> v .: "email"
         "Login" -> QuestionLogin <$> v .: "name" <*> v .: "password"
         "RoomsList" -> return QuestionRoomsList
         "SubscribeToRoom" -> QuestionSubscribeToRoom <$> v .: "name"
         "ActiveUsersList" -> return QuestionActiveUsersList
         "ActiveGamesList" -> return QuestionActiveGamesList
         "RoomMessage" -> QuestionRoomMessage <$> v .: "name" <*> v .: "text"
         "UsersCount" -> return QuestionUsersCount
         "GamesCount" -> return QuestionGamesCount
         "UsersList" -> QuestionUsersList <$> v .: "page" <*> v .: "pageSize"
         "GamesList" -> QuestionGamesList <$> v .: "page" <*> v .: "pageSize"
         "Request" -> QuestionRequest <$> v .: "settings"
         "CancelRequest" -> return QuestionCancelRequest
         _ -> mzero
  parseJSON _ = mzero
