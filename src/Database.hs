module Database where

import Data.Int
import Control.Monad
import Control.Applicative
import Data.Maybe
import Database.Persist.Sqlite
import Data.Pool

fromKey :: ToBackendKey SqlBackend record => Key record -> Int64
fromKey = fromSqlKey

insert' :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> a -> IO (Key a)
insert' pool = flip runSqlPersistMPool pool . insert

get' :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> Key a -> IO (Maybe a)
get' pool = flip runSqlPersistMPool pool . get

getBy' :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> Unique a -> IO (Maybe (Entity a))
getBy' pool = flip runSqlPersistMPool pool . getBy

getAll :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> IO [Entity a]
getAll pool = flip runSqlPersistMPool pool $ selectList [] []

getAllVals :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> IO [a]
getAllVals = liftM (map entityVal) . getAll

exists :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> Key a -> IO Bool
exists pool = liftM isJust . flip runSqlPersistMPool pool . get

existsBy :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> Unique a -> IO Bool
existsBy pool = liftM isJust . flip runSqlPersistMPool pool . getBy

getMaybeByIds :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Key a] -> IO [Maybe (Entity a)]
getMaybeByIds pool = flip runSqlPersistMPool pool . mapM (\i -> fmap (Entity i) <$> get i)

getMaybeValsByIds :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Key a] -> IO [Maybe a]
getMaybeValsByIds pool = flip runSqlPersistMPool pool . mapM get

getByIds :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Key a] -> IO [Entity a]
getByIds pool = fmap (map fromJust) . getMaybeByIds pool

getValsByIds :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Key a] -> IO [a]
getValsByIds pool = fmap (map fromJust) . getMaybeValsByIds pool

selectList' :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Filter a] -> [SelectOpt a] -> IO [Entity a]
selectList' pool filters = flip runSqlPersistMPool pool . selectList filters

selectValsList :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Pool SqlBackend -> [Filter a] -> [SelectOpt a] -> IO [a]
selectValsList pool filters = liftM (map entityVal) . selectList' pool filters

pageSelectOpts :: Int -> Int -> [SelectOpt a]
pageSelectOpts page pageSize = [LimitTo pageSize, OffsetBy $ (page - 1) * pageSize]

count' :: (PersistEntity val, PersistEntityBackend val ~ SqlBackend) => Pool SqlBackend -> [Filter val] -> IO Int
count' pool = flip runSqlPersistMPool pool . count
