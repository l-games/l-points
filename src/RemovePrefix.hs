module RemovePrefix where

import Data.Char

removePrefix :: Int -> String -> String
removePrefix n s = let s' = drop n s in toLower (head s') : tail s'
