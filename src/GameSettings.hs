module GameSettings where

import Data.Aeson.TH
import RemovePrefix

data GameSettings = GameSettings { gameSettingsWidth :: Int,
                                   gameSettingsHeight :: Int,
                                   gameSettingsSecondsAtStart :: Int,
                                   gameSettingsSecondsPerMove :: Int,
                                   gameSettingsRating :: Bool }

$(deriveJSON defaultOptions { fieldLabelModifier = removePrefix 12 } ''GameSettings)
