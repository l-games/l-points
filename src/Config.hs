module Config where

import Data.Text

minUserNameLength :: Int
minUserNameLength = 1

maxUserNameLength :: Int
maxUserNameLength = 30

minPasswordLength :: Int
minPasswordLength = 6

maxPasswordLength :: Int
maxPasswordLength = 50

minMessageLength :: Int
minMessageLength = 1

maxMessageLength :: Int
maxMessageLength = 500

serverAddress :: String
serverAddress = "0.0.0.0"

serverPort :: Int
serverPort = 9160

dbFile :: Text
dbFile = "db.sqlite"

dbPoolSize :: Int
dbPoolSize = 10

modelsFile :: String
modelsFile = "src/models"

adminName :: Text
adminName = "admin"

adminPassword :: Text
adminPassword = "admin"

adminEmail :: Text
adminEmail = "admin@admin.org"

protocolVersion :: Int
protocolVersion = 0

busyAtLogin :: Bool
busyAtLogin = False

startElo :: Double
startElo = 800

minWidth :: Int
minWidth = 15

minHeight :: Int
minHeight = 15

maxWidth :: Int
maxWidth = 50

maxHeight :: Int
maxHeight = 50

minSecondsAtStart :: Int
minSecondsAtStart = 10

maxSecondsAtStart :: Int
maxSecondsAtStart = 7200

minSecondsPerMove :: Int
minSecondsPerMove = 0

maxSecondsPerMove :: Int
maxSecondsPerMove = 300
