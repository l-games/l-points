module Server where

import Data.Maybe
import Data.Time
import Data.Aeson
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BS
import Data.Text.Encoding
import Control.Monad
import Control.Concurrent.MVar
import qualified Data.Map as M
import qualified Data.Set as S
import Database.Persist.Sqlite
import Config
import Player
import Field
import Question
import Answer
import PersistentEnums
import PersistentModel
import Database
import GameSettings

type Sender = Answer -> IO ()

type Receiver = IO Question

type TextSender = T.Text -> IO ()

type TextReceiver = IO T.Text

type ClientKey = Int

type UserKey = Key User

type GameKey = Key Game

type RoomKey = Key Room

data UserStatus = UserStatus { serverUserBusy :: Bool }

data GameStatus = GameStatus { serverGameField :: Field,
                               serverGameCurPlayer :: Player,
                               serverGameRedMillis :: Int,
                               serverGameBlackMullis :: Int,
                               serverGameRedLastMoveTime :: UTCTime,
                               serverGameBlackLastMoveTime :: UTCTime }

data ServerState = ServerState { serverStateGames :: M.Map GameKey GameStatus,
                                 serverStateLogins :: M.Map ClientKey UserKey,
                                 serverStateUsers :: M.Map UserKey UserStatus,
                                 serverStateConnections :: M.Map ClientKey (Sender, Receiver),
                                 serverStateGamesSubscribers :: M.Map GameKey (S.Set ClientKey),
                                 serverStateRoomsSubscribers :: M.Map RoomKey (S.Set ClientKey),
                                 serverStateGameRequests :: M.Map UserKey GameSettings, -- Ключ - id пользователя, оставившего запрос.
                                 serverStatePersonalGameRequests :: M.Map UserKey (UserKey, GameSettings) } -- Ключ - id пользователя, к которому пришел запрос.

nextClientKey :: S.Set ClientKey -> ClientKey
nextClientKey ids = S.findMin $ (S.insert 0 $ S.map (+ 1) ids) S.\\ ids

newServerState :: ServerState
newServerState = ServerState M.empty M.empty M.empty M.empty M.empty M.empty M.empty M.empty

defaultUserStatus :: UserStatus
defaultUserStatus = UserStatus busyAtLogin

sendAll :: Answer -> [Sender] -> IO ()
sendAll ans = mapM_ (\f -> f ans)

senders :: ServerState -> [Sender]
senders = map fst . M.elems . serverStateConnections

fromTextSender :: TextSender -> Sender
fromTextSender = (. decodeUtf8 . BS.toStrict . encode)

fromTextReceiver :: Sender -> TextReceiver -> Receiver
fromTextReceiver sender receiver =
  do msg <- receiver
     case decodeStrict $ encodeUtf8 msg of
       Nothing -> do sender $ AnswerError ErrorAnswerMessageFormat
                     fromTextReceiver sender receiver
       Just question -> return question

connect :: MVar ServerState -> Sender -> Receiver -> IO ClientKey
connect mstate sender receiver =
  do state <- takeMVar mstate
     let oldServerConnections = serverStateConnections state
         clientKey = nextClientKey $ M.keysSet oldServerConnections
         newState = state { serverStateConnections = M.insert clientKey (sender, receiver) oldServerConnections }
     putMVar mstate newState
     return clientKey

disconnect :: MVar ServerState -> ClientKey -> IO () -- оповестить всех о выходе, удалении заявок
disconnect mstate clientKey = 
  do state <- takeMVar mstate
     let maybeUserId = M.lookup clientKey $ serverStateLogins state
         newState = state { serverStateConnections = M.delete clientKey $ serverStateConnections state,
                            serverStateLogins = if isNothing maybeUserId
                                                then serverStateLogins state
                                                else M.delete clientKey $ serverStateLogins state,
                            serverStateGamesSubscribers = M.map (S.delete clientKey) $ serverStateGamesSubscribers state,
                            serverStateRoomsSubscribers = M.map (S.delete clientKey) $ serverStateRoomsSubscribers state,
                            serverStateUsers = if isNothing maybeUserId
                                               then serverStateUsers state
                                               else M.delete (fromJust maybeUserId) $ serverStateUsers state }
     putMVar mstate newState

gameToSettings :: Game -> GameSettings
gameToSettings game =
  GameSettings { gameSettingsWidth = gameWidth game,
                 gameSettingsHeight = gameHeight game,
                 gameSettingsSecondsAtStart = gameSecondsAtStart game,
                 gameSettingsSecondsPerMove = gameSecondsPerMove game,
                 gameSettingsRating = gameRating game }

toAnswerUser :: User -> AnswerUser
toAnswerUser user =
  AnswerUser { answerUserName = userName user,
               answerUserEmail = userEmail user,
               answerUserRole = userRole user }

toAnswerActiveUser :: User -> UserStatus -> AnswerActiveUser
toAnswerActiveUser user userStatus =
  AnswerActiveUser { answerActiveUserName = userName user,
                     answerActiveUserEmail = userEmail user,
                     answerActiveUserRole = userRole user,
                     answerActiveUserBusy = serverUserBusy userStatus }

toAnswerGame :: GameKey -> Game -> T.Text -> T.Text -> AnswerGame
toAnswerGame gameKey game redName blackName =
  AnswerGame { answerGameId = fromIntegral $ fromKey gameKey,
               answerGameRed = redName,
               answerGameBlack = blackName,
               answerGameSettings = gameToSettings game }

userKeysFromGames :: [Entity Game] -> [UserKey]
userKeysFromGames = S.toList . S.fromList . concat . map (\game -> [gameRedId game, gameBlackId game]) . map entityVal

toAnswerGames :: [Entity Game] -> M.Map UserKey T.Text -> [AnswerGame]
toAnswerGames games userNamesMap =
  map (\(Entity gameKey game) -> let redName = userNamesMap M.! (gameRedId game)
                                     blackName = userNamesMap M.! (gameBlackId game)
                                 in toAnswerGame gameKey game redName blackName) games

isSettingsValid :: GameSettings -> Bool
isSettingsValid settings | gameSettingsWidth settings < minWidth = False
                         | gameSettingsWidth settings > maxWidth = False
                         | gameSettingsHeight settings < minHeight = False
                         | gameSettingsHeight settings > maxHeight = False
                         | gameSettingsSecondsAtStart settings < minSecondsAtStart = False
                         | gameSettingsSecondsAtStart settings > maxSecondsAtStart = False
                         | gameSettingsSecondsPerMove settings < minSecondsPerMove = False
                         | gameSettingsSecondsPerMove settings > maxSecondsPerMove = False
                         | otherwise = True

listen :: MVar ServerState -> ConnectionPool -> ClientKey -> Sender -> Receiver -> IO ()
listen mstate pool clientKey sender receiver = forever $
  do question <- receiver
     case question of
       QuestionProtocolVersion ->
         sender $ AnswerOk $ OkAnswerProtocolVersion protocolVersion
       QuestionRegister name password email ->
         if | T.length name < minUserNameLength     -> sender $ AnswerError ErrorAnswerTooSmallUserName
            | T.length name > maxUserNameLength     -> sender $ AnswerError ErrorAnswerTooBigUserName
            | T.length password < minPasswordLength -> sender $ AnswerError ErrorAnswerTooSmallPassword
            | T.length password > maxPasswordLength -> sender $ AnswerError ErrorAnswerTooBigPassword
            | otherwise ->
              do maybeUser <- getBy' pool $ UniqueUserName name
                 if isJust maybeUser
                   then sender $ AnswerError ErrorAnswerUserAlreadyExists
                   else do curTime <- getCurrentTime
                           insert' pool $ User name password email UserRoleRegular False curTime startElo startElo
                           sender $ AnswerOk OkAnswerRegistered
       QuestionLogin name password -> -- Оповестить всех о входе. Сделать выход.
         do maybeUser <- getBy' pool $ UniqueUserName name
            case maybeUser of
              Nothing -> sender $ AnswerError ErrorAnswerUserNotFound
              Just (Entity userKey user) ->
                if | userPassword user /= password -> sender $ AnswerError ErrorAnswerIncorrectPassword
                   | userBanned user -> sender $ AnswerError ErrorAnswerUserBanned
                   | otherwise ->
                     do state <- takeMVar mstate
                        let maybeUserId = M.lookup clientKey $ serverStateLogins state
                            newState = state { serverStateLogins = M.insert clientKey userKey $ serverStateLogins state,
                                               serverStateUsers = M.insert userKey defaultUserStatus $ serverStateUsers state }
                        case maybeUserId of
                          Just _  -> do putMVar mstate state
                                        sender $ AnswerError ErrorAnswerAlreadyLogined
                          Nothing -> do putMVar mstate newState
                                        sender $ AnswerOk OkAnswerLogined
       QuestionRoomsList ->
         do rooms <- getAllVals pool
            sender $ AnswerOk $ OkAnswerRooms $ map roomName rooms
       QuestionSubscribeToRoom name ->
         do maybeRoom <- getBy' pool $ UniqueRoomName name
            case maybeRoom of
              Nothing                 -> sender $ AnswerError ErrorAnswerRoomNotFound
              Just (Entity roomKey _) ->
                do state <- takeMVar mstate
                   let newState = state { serverStateRoomsSubscribers = M.alter (\case
                         Nothing  -> Just $ S.singleton clientKey
                         Just set -> Just $ S.insert clientKey set) roomKey $ serverStateRoomsSubscribers state }
                   putMVar mstate newState
                   sender $ AnswerOk OkAnswerSubscribed
       QuestionActiveUsersList ->
         do state <- readMVar mstate
            let keys = M.elems $ serverStateLogins state
                serverUsers = map (serverStateUsers state M.!) keys
            users <- getValsByIds pool keys
            sender $ AnswerOk $ OkAnswerActiveUsers $ zipWith toAnswerActiveUser users serverUsers
       QuestionActiveGamesList ->
         do state <- readMVar mstate
            games <- getByIds pool $ M.keys $ serverStateGames state
            let userKeys = userKeysFromGames games
            users <- getValsByIds pool userKeys
            let userNamesMap = M.fromList $ zip userKeys $ map userName users
            sender $ AnswerOk $ OkAnswerActiveGames $ toAnswerGames games userNamesMap
       QuestionRoomMessage name text ->
         if | T.length text < minMessageLength -> sender $ AnswerError ErrorAnswerTooSmallMessage
            | T.length text > maxMessageLength -> sender $ AnswerError ErrorAnswerTooBigMessage
            | otherwise ->
              do maybeRoom <- getBy' pool $ UniqueRoomName name
                 case maybeRoom of
                   Nothing                 -> sender $ AnswerError ErrorAnswerRoomNotFound
                   Just (Entity roomKey _) ->
                     do state <- readMVar mstate
                        let maybeUserKey = M.lookup clientKey $ serverStateLogins state
                        case maybeUserKey of
                          Nothing      -> sender $ AnswerError ErrorAnswerAccessDenied
                          Just userKey ->
                            do curTime <- getCurrentTime
                               insert' pool $ RoomMessage userKey roomKey text curTime
                               sender $ AnswerOk OkAnswerSended
                               sendAll (AnswerDelivery $ DeliveryAnswerRoomMessage name text) $ senders state
       QuestionUsersCount ->
         do c <- count' pool ([] :: [Filter User])
            sender $ AnswerOk $ OkAnswerUsersCount c
       QuestionGamesCount ->
         do c <- count' pool ([] :: [Filter Game])
            sender $ AnswerOk $ OkAnswerGamesCount c
       QuestionUsersList page pageSize -> --проверить границы
         do users <- selectValsList pool [] (Asc UserName : pageSelectOpts page pageSize)
            sender $ AnswerOk $ OkAnswerUsers $ map toAnswerUser users
       QuestionGamesList page pageSize -> --проверить границы
         do games <- selectList' pool [] (Desc GameStartTime : pageSelectOpts page pageSize)
            let userKeys = userKeysFromGames games
            users <- getValsByIds pool userKeys
            let userNamesMap = M.fromList $ zip userKeys $ map userName users
            sender $ AnswerOk $ OkAnswerGames $ toAnswerGames games userNamesMap
       QuestionRequest gameSettings ->
         if not $ isSettingsValid gameSettings
         then sender $ AnswerError ErrorAnswerInvalidSettings
         else do state <- takeMVar mstate
                 let maybeUserKey = M.lookup clientKey $ serverStateLogins state
                 case maybeUserKey of
                   Nothing      -> do putMVar mstate state
                                      sender $ AnswerError ErrorAnswerAccessDenied
                   Just userKey ->
                     let maybeRequest = M.lookup userKey $ serverStateGameRequests state
                         newState = state { serverStateGameRequests = M.insert userKey gameSettings $ serverStateGameRequests state }
                     in case maybeRequest of
                          Just _  -> do putMVar mstate state
                                        sender $ AnswerError ErrorAnswerRequestAlreadyExists
                          Nothing ->
                            do putMVar mstate newState
                               sender $ AnswerOk OkAnswerRequestCreated
                               Just user <- get' pool userKey
                               sendAll (AnswerDelivery $ DeliveryAnswerRequestCreated (userName user) gameSettings) $ senders state
       QuestionCancelRequest ->
         do state <- takeMVar mstate
            let maybeUserKey = M.lookup clientKey $ serverStateLogins state
            case maybeUserKey of
              Nothing      -> do putMVar mstate state
                                 sender $ AnswerError ErrorAnswerAccessDenied
              Just userKey ->
                let maybeRequest = M.lookup userKey $ serverStateGameRequests state
                    newState = state { serverStateGameRequests = M.delete userKey $ serverStateGameRequests state }
                in case maybeRequest of
                     Nothing -> do putMVar mstate state
                                   sender $ AnswerError ErrorAnswerRequestDoesNotExist
                     Just _  ->
                       do putMVar mstate newState
                          sender $ AnswerOk OkAnswerRequestCanceled
                          Just user <- get' pool userKey
                          sendAll (AnswerDelivery $ DeliveryAnswerRequestCanceled $ userName user) $ senders state
