module Main where

import Control.Monad
import Data.Time
import Database.Persist.Sqlite
import Control.Exception
import Control.Monad.IO.Class
import Control.Monad.Logger
import qualified Network.WebSockets as WS
import Control.Concurrent.MVar
import System.Environment
import Config
import Server
import PersistentEnums
import PersistentModel

getPreferences :: [String] -> Bool -> ([String], Bool)
getPreferences [] migrateOpt = ([], migrateOpt)
getPreferences args@(h : t) migrateOpt =
  case h of
    "--migrate" -> getPreferences t True
    _ -> (args, migrateOpt)

application :: MVar ServerState -> ConnectionPool -> WS.ServerApp
application mstate pool pending =
  do connection <- WS.acceptRequest pending -- Записать в базу ip.
     let sender = fromTextSender $ WS.sendTextData connection
         receiver = fromTextReceiver sender $ WS.receiveData connection
     clientId <- connect mstate sender receiver
     flip finally (disconnect mstate clientId) $ listen mstate pool clientId sender receiver

main :: IO ()
main =
  do args <- getArgs
     let (_, migrateOpt) = getPreferences args False
     if migrateOpt
       then do curTime <- getCurrentTime
               runSqlite dbFile $ do runMigration migrateAll
                                     admin <- getBy $ UniqueUserName adminName
                                     case admin of
                                       Nothing -> void $ insert $ User adminName adminPassword adminEmail UserRoleAdministrator False curTime startElo startElo
                                       Just _  -> return ()
       else runStderrLoggingT $ withSqlitePool dbFile dbPoolSize $ \pool -> liftIO $
              do mstate <- newMVar newServerState
                 WS.runServer serverAddress serverPort $ application mstate pool
